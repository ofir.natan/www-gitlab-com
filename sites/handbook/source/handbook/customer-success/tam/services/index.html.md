---
layout: handbook-page-toc
title: "TAM Responsibilities and Services"
description: "There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## TAM Alignment

For an overview of TAM segments by ARR, please see [this wiki page](https://gitlab.com/gitlab-com/customer-success/tam/-/wikis/Segments)(Internal GitLab only)

## Responsibilities and Services

There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilization of GitLab's products and services. These services typically include the following. Please note this list is not definitive, and more services may be provided than listed or some may not be offered, depending on the size and details of the account.

### Relationship Management

- Scheduled cadence calls (synchronous)
- Regular open issue reviews and issue escalations (asynchronous)
- Success strategy roadmaps - beginning with an onboarding success plan, for example a 30/60/90 day plan
- Focus on adoption roadmap and milestones in-line with desired business outcomes and intended use cases.
- Executive business reviews (1-2 times a year for Enterprise; as needed for Commercial)
- Partnership into expansion into new use cases and associated enablement & guidance
- Internal Advocacy: TAM is the customer champion for guidance and requests, a liaison between the customer and other GitLab teams


### Training

- Identification of pain points and training required
- Coordination of demos and training sessions, potentially delivered by the Technical Account Manager if time and technical knowledge allows
- Deliver the use case enablement sessions/workshops/lunch & learns to ensure the teams, leadership and end users are setup for success and adopting the platform successfully 
- Regular communication and updates on GitLab features
- Product and feature guidance - new feature presentations

### Support

- Upgrade planning (in partnership with Support)
  - Review [Live Upgrade Assistance page](/support/scheduling-live-upgrade-assistance.html#how-do-i-schedule-live-upgrade-assistance) with customers when upgrading to ensure a plan is in place(including rollback strategy) and Support have enough preparation time to be available to help.
- [Infrastructure upgrade coordination](/handbook/customer-success/tam/services/infrastructure-upgrade/)
- Launch best practices
- Advocate on support escalations
- Monitor SaaS based customer experience by adding them to the [Marquee Accounts alerts](https://gitlab.com/gitlab-com/gl-infra/marquee-account-alerts) project

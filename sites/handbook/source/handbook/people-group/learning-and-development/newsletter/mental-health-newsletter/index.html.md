---
layout: handbook-page-toc
title: Mental Health Newsletter
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Overview

In 2020 and 2021, the Learning and Development team at GitLab used a quarterly newsletter to educate the team on how they can [manage burnout](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/24). This quarterly newsletter was part of a larger effort to increase conversation about taking care of our mental health, increasing awareness of existing resources available to the GitLab team, and enable managers to support their team.

The final edition of the newsletter was shared in FY22 Q4. All past newsletter can be found [in the handbook](https://about.gitlab.com/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/#past-newsletters)

### Long term goals

1. Normalize the discussion about mental health across the GitLab team.
1. Increase access to and awareness of existing mental health resources for the GitLab team.
1. Understand how our asynchronous culture can contribute to improved mental health for team members, specifically by decreasing the total number of synchronous meetings for team members.
1. Increase documentation about mental health management resources in our handbook by collaborating and sharing strategies and tools that work


## Newsletter process

This newsletter is shared on the third to last Thursday of each quarter. This cadence comes 3 weeks before the [L&D newsletter](/handbook/people-group/learning-and-development/newsletter/) is shared, to avoid confusion or an overload of information.

The newsletter is planned using both issues in the [Mental Health project](https://gitlab.com/gitlab-com/people-group/learning-development/mental-health) and merge requests to the `www-gitlab-com` project. The planning process includes the following steps:

1. Open a WIP merge request to begin a new newsletter page 3 weeks before publish date
1. Open an issue to hold discussion and feedback related to the newsletter
1. Relevant stakeholders/contributors are tagged in the merge request each month to provide content or review suggested content in the outline
1. Reviewers provide feedback no later than 3 pm CT two business days before the planned go-live date to allow time for revisions
1. L&D Team announces the newsletter via slack on go-live date.
1. Once the newsletter goes live, the L&D Team sends a slack notification in the #whats-happening-at-gitlab, #learninganddevelopment, and #managers Slack channels

## Past newsletters

1. [FY21-Q4 Mental Health Newsletter](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY21-Q4/)
1. [FY22-Q1 Mental Health Newsletter](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q1/)
1. [FY22-Q2 Mental Health Newsletter](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q2/)
1. [FY22-Q3 Mental Health Newsletter](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter/FY22-Q3/)
1. [FY22-Q4 Mental Health Newsletter]
```
